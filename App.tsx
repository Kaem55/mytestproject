/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from "react";
import { Platform, StyleSheet, View, StatusBar } from "react-native";
import { Dimensions } from "react-native";
import { Provider } from "react-redux";
import configureStore from "./src/configureStore";
import { AppNavigator } from "./src/navigator";
import requestReadWriteStoragePermission from "./src/utils/permissions";
import ToastService from "src/services/ToastService";
import Toast, { DURATION } from "react-native-easy-toast";

if (__DEV__) {
  import("./src/ReactotronConfig").then(() =>
    console.log("Reactotron Configured")
  );
}

const deviceWidth = Dimensions.get("window").width;

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

const store = configureStore();
const permissionsGranted = requestReadWriteStoragePermission();

export default () => (
  <View style={styles.container}>
    <StatusBar barStyle="light-content" />
    <Provider store={store}>
      <React.Fragment>
        <AppNavigator />
        <Toast
          ref={toastRef => {
            ToastService.setTopLevelToast(toastRef);
          }}
          style={{ backgroundColor: "#e4e3e3" }}
          textStyle={{ color: "#8a8d91" }}
          fadeInDuration={250}
          useNativeAnimation={true}
        />
      </React.Fragment>
    </Provider>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  }
});
