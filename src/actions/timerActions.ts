import {
  START_BACKGROUND_TIMER,
  STOP_BACKGROUND_TIMER
} from "../constants/actionTypes";

export const startTimer = delay => ({
  type: START_BACKGROUND_TIMER,
  delay
});

export const stopTimer = () => ({
  type: STOP_BACKGROUND_TIMER
});
