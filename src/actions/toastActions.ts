import { SHOW_TOAST, CLOSE_TOAST } from "../constants/actionTypes";

export const showToast = (time: number = 1000, native?: boolean) => ({
  type: SHOW_TOAST,
  time,
  native
});

export const closeToast = () => ({
  type: CLOSE_TOAST
});
