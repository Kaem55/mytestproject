import { APPSTATE_EVENT } from "../constants/actionTypes";

export const appStateEvent = status => ({
  type: APPSTATE_EVENT,
  status
});
