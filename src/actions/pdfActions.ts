import * as types from "../constants/actionTypes";

export const scrollPdfChanged = scroll => ({
  type: types.SCROLL_CHANGED,
  scroll
});

export const zoomChanged = zoom => ({
  type: types.ZOOM_CHANGED,
  zoom
});
