import { LOGIN_FETCHING_DATA } from "../constants/actionTypes";

export function fetchLoginData() {
  console.log("fetchLoginData");
  return {
    type: LOGIN_FETCHING_DATA
  };
}

export function resetData() {
  console.log("action resetData");
  return {
    type: "RESET"
  };
}
