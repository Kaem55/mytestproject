import * as types from "../constants/actionTypes";

let nextMessageId = 0;
let nextUserId = 0;

export const openConnection = () => ({
  type: types.CHAT_CONNECTION_OPEN
});

export const openConnectionSuccess = () => ({
  type: types.CHAT_CONNECTION_OPEN_SUCCESS
});

export const openConnectionFailure = () => ({
  type: types.CHAT_CONNECTION_OPEN_FAILURE
});

export const closeConnection = () => ({
  type: types.CHAT_CONNECTION_CLOSE
});

export const addMessage = (message, author, avatar) => ({
  type: types.ADD_MESSAGE,
  id: nextMessageId++,
  message,
  author,
  avatar
});

export const addUser = name => ({
  type: types.ADD_USER,
  id: nextUserId++,
  name
});

export const messageReceived = (message, author, avatar) => ({
  type: types.MESSAGE_RECEIVED,
  id: nextMessageId++,
  message,
  author,
  avatar
});

export const populateUsersList = users => ({
  type: types.USERS_LIST,
  users
});

export const typingChanged = typing => ({
  type: types.TYPING_CHANGED,
  typing
});
