import {
  HOME_FETCHING_DATA,
  CHAT_CONNECTION_OPEN
} from "../constants/actionTypes";

export function fetchHomeData() {
  console.log("fetchHomeData");
  return {
    type: HOME_FETCHING_DATA
  };
}

export const openConnection = () => ({
  type: CHAT_CONNECTION_OPEN
});
