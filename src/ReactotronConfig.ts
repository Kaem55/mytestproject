import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";
import sagaPlugin from "reactotron-redux-saga";

const reactotron = Reactotron.configure({
  name: "Test App",
  host: "172.20.1.113"
}) // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .use(reactotronRedux())
  .use(sagaPlugin({}))
  .connect(); // let's connect!

export default reactotron;

/*const sagaMonitor = Reactotron.createSagaMonitor;
export { sagaMonitor };
*/
