const BASE_API_URL = "https://s3-eu-west-1.amazonaws.com";

export function getHomeList() {
  return request("backstage-dev-alex/glowblcatusers.json");
}

function request(endpoint: string) {
  return fetch(`${BASE_API_URL}/${endpoint}`).then(raw => raw.json());
}

const loginData: String = "login complete";
export function getLoginData(): any {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve(loginData);
    }, 3000);
  });
}
