import Toast, { DURATION } from "react-native-easy-toast";
import { ToastAndroid, Platform } from "react-native";

let _toast: Toast;

function setTopLevelToast(toastRef: Toast | null) {
  _toast = toastRef as Toast;
}

function show(
  message: string,
  duration: number = 1000,
  useNative?: boolean
): void {
  console.log("show toast", message, duration);
  if (useNative && Platform.OS === "android") {
    ToastAndroid.show(message, duration);
  } else {
    _toast.show(message, duration);
  }
}

function close(): void {
  _toast.close();
}

export default { show, close, setTopLevelToast };
