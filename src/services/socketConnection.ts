import { Dispatch } from "redux";
import * as actions from "../actions/chatActions";
import * as types from "../constants/actionTypes";

export default class SocketConnection {
  private ws!: WebSocket;
  private dsp: Dispatch;
  private _isConnected!: boolean;

  private Emitter = require("tiny-emitter");
  public emitter = new this.Emitter();

  constructor(storeDispatch: Dispatch) {
    this.dsp = storeDispatch;
  }

  public isConnected(): boolean {
    return this._isConnected;
  }

  public open(): void {
    this.internalOpen();
  }

  public close(): void {
    this.internalClose();
  }

  public send(action: any): void {
    console.log("SocketConnection - send action: ", action);
    if (this.ws != null) {
      this.ws.send(JSON.stringify(action));
    }
  }

  private internalOpen(): void {
    console.info("SocketConnection - start connection");
    this.ws = new WebSocket("wss://node2.wsninja.io");
    this.ws.onopen = () => {
      // connection opened
      this.ws.send(
        JSON.stringify({ guid: "79340ddb-ef21-4b7f-9704-5243a10e78d2" })
      );
      this.dsp(actions.openConnectionSuccess());
    };

    this.ws.onmessage = event => {
      // a message was received
      console.log("SocketConnection - onmessage ", event.data);
      const data = JSON.parse(event.data);
      if (data.accepted === true) {
        console.log("SocketConnection - wsninja connection accepted");
        return;
      }
      switch (data.type) {
        case types.ADD_MESSAGE:
          this.emitter.emit(
            "message-event",
            data.message,
            data.author,
            data.avatar
          );
          //this.dsp(
          // actions.messageReceived(data.message, data.author, data.avatar)
          //);
          break;
        case types.ADD_USER:
          this.dsp(actions.addUser(data.name));
          break;
      }
    };

    this.ws.onerror = event => {
      // an error occurred
      //console.log(e.message);
      this.dsp(actions.openConnectionFailure());
    };

    this.ws.onclose = event => {
      // connection closed
      console.log(event.code, event.reason);
      this.dsp(actions.openConnectionFailure());
    };
  }

  private internalClose(): void {
    console.info("SocketConnection - close connection");
    this.ws.close();
  }
}
