import { takeEvery } from "redux-saga/effects";
import { SHOW_TOAST, CLOSE_TOAST } from "../constants/actionTypes";
import ToastService from "../services/ToastService";
import { AnyAction } from "redux";

function* showToastSaga(action: AnyAction) {
  console.log("showToastSaga", action);
  const { time, native } = action;
  ToastService.show("this is a test toast", time, native);
}

function* closeToastSaga() {
  ToastService.close();
}

export default function* toastRootSaga() {
  yield takeEvery(SHOW_TOAST, showToastSaga);
  yield takeEvery(CLOSE_TOAST, closeToastSaga);
}
