import { CANCEL } from "redux-saga";
import BackgroundTimer from "../../../utils/timerUtils";

export default function delaySP(ms) {
  let timeoutId;
  const promise = new Promise(resolve => {
    timeoutId = BackgroundTimer.setTimeout(resolve, ms);
  });

  promise[CANCEL] = () => {
    BackgroundTimer.clearTimeout(timeoutId);
  };

  return promise;
}
