import { call, put, take, fork, cancel, cancelled } from "redux-saga/effects";
import { Channel, eventChannel } from "redux-saga";
import { AppState, AppStateEvent, AppStateStatus } from "react-native";
import { appStateEvent } from "../actions/appStateActions";

function* stateWatcher() {
  const channel: Channel<AppStateStatus> = subscribe("change");
  try {
    while (true) {
      const status = yield take(channel);
      yield put(appStateEvent(status));
    }
  } catch (err) {
  } finally {
    channel.close();
  }
}

function subscribe(eventType: AppStateEvent): Channel<AppStateStatus> {
  let isAlive: boolean = true;
  return eventChannel<AppStateStatus>(emit => {
    const subscribe = AppState.addEventListener(eventType, nextAppState => {
      emit(nextAppState);
    });
    const unsubscribe = () => {
      isAlive = false;
    };
    return unsubscribe;
  });
}

export default function* rootAppStateSagas() {
  yield stateWatcher();
}
