import { call, put, take, fork, cancel, cancelled } from "redux-saga/effects";
import {
  START_BACKGROUND_TIMER,
  TIMER_EVENT,
  STOP_BACKGROUND_TIMER
} from "../constants/actionTypes";
import delaySP from "./effects/delaySP";

function* tick() {
  try {
    let tickCount: number = 0;
    while (true) {
      yield call(delaySP, 1000);
      yield put({ type: TIMER_EVENT });
      console.log("Timer tick :", tickCount++);
    }
  } finally {
    if (yield cancelled()) {
      // clean timer
    }
  }
}

function* timer() {
  while (yield take(START_BACKGROUND_TIMER)) {
    console.log("Start timer");
    // starts the task in the background
    const bgSyncTask = yield fork(tick);

    // wait for the user stop action
    yield take(STOP_BACKGROUND_TIMER);
    console.log("Stop timer");
    // user clicked stop. cancel the background task
    // this will throw a SagaCancellationException into task
    yield cancel(bgSyncTask);
  }
}

export default function* rootTimerSaga() {
  yield timer();
}
