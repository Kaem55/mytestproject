import {
  HOME_FETCHING_DATA,
  HOME_FETCHING_DATA_SUCCESS,
  HOME_FETCHING_DATA_FAILURE,
  LOGIN_FETCHING_DATA,
  LOGIN_FETCHING_DATA_SUCCESS,
  LOGIN_FETCHING_DATA_FAILURE,
  CHAT_CONNECTION_OPEN,
  CHAT_CONNECTION_OPEN_FAILURE,
  ADD_MESSAGE,
  CHAT_CONNECTION_CLOSE,
  START_BACKGROUND_TIMER,
  STOP_BACKGROUND_TIMER,
  TIMER_EVENT
} from "../constants/actionTypes";
import * as actions from "../actions/chatActions";
import { put, takeEvery, take, call, fork } from "redux-saga/effects";
import { SagaIterator, eventChannel, buffers, delay } from "redux-saga";
import { getHomeList, getLoginData } from "../services/homeService";
import SocketConnection from "../services/socketConnection";
import rootTimerSaga from "./timerSagas";
import toastRootSaga from "./toastSagas";

function* watchSocketConnection(action: any) {
  console.log("watchSocketConnection :", action.type);
  let channel;
  if (action.type == CHAT_CONNECTION_OPEN) {
    try {
      yield connection.open();
    } catch (e) {
      yield put({ type: CHAT_CONNECTION_OPEN_FAILURE });
    }
  } else if (action.type == CHAT_CONNECTION_CLOSE) {
    try {
      if (channel) channel.close();
      yield connection.close();
    } catch (e) {
      //yield put({ type: CHAT_CONNECTION_OPEN_FAILURE });
    }
  }

  channel = yield call(createEventChannel, connection.emitter);
  console.log("socket connection open");

  yield delay(15000);

  console.log("start listen messages");

  //try {
  while (true) {
    const { message, author, avatar } = yield take(channel);
    console.log("message received from channel :", message);
    if (!message) {
      break;
    }
    yield put(actions.messageReceived(message, author, avatar));
    //this.dsp(
    // actions.messageReceived(data.message, data.author, data.avatar)
    //);
  }
  /*
}
finally {
    if (yield cancelled()) {
      channel.close()
      console.log('countdown cancelled')
   }
   */
}

function createEventChannel(socketEmitter) {
  return eventChannel(
    emit => {
      function messageHandler(message, author, avatar) {
        console.log(
          "event channel message received :",
          message,
          author,
          avatar
        );
        emit({ message, author, avatar });
      }
      // call emit when a message is received
      socketEmitter.on("message-event", function(message, author, avatar) {
        messageHandler(message, author, avatar);
      });
      // Return a function to be called when done listening
      return () => {
        socketEmitter.off();
      };
    },
    buffers.sliding(5),
    filterMessages
  );
}

function filterMessages(arg: any): boolean {
  console.log("filterMessages ", arg);
  let isPush: boolean = false;
  if (arg.author == "alex" || arg.author == "seb") isPush = true;
  return isPush;
}

function* watchSendMessage(action: any) {
  try {
    yield connection.send(action);
  } catch (e) {
    //yield put({ type: CHAT_CONNECTION_OPEN_FAILURE });
  }
}

function* fetchHomeData(action: any): SagaIterator {
  try {
    const data = yield getHomeList();
    yield put({ type: HOME_FETCHING_DATA_SUCCESS, data });
  } catch (e) {
    yield put({ type: HOME_FETCHING_DATA_FAILURE });
  }
}

function* fetchLoginData(action: any): SagaIterator {
  console.log("Saga - fetchLoginData");
  try {
    const data = yield getLoginData();
    yield put({ type: LOGIN_FETCHING_DATA_SUCCESS, data });
  } catch (e) {
    yield put({ type: LOGIN_FETCHING_DATA_FAILURE });
  }
}

function* keepAliveSaga(action: any): SagaIterator {
  console.log("on timer tick event");
}

let connection: SocketConnection;

export default function* rootSaga(params): SagaIterator {
  console.log("root Saga params: ", params);
  connection = params;
  yield fork(rootTimerSaga);
  yield fork(toastRootSaga);
  yield takeEvery(TIMER_EVENT, keepAliveSaga);
  yield takeEvery(LOGIN_FETCHING_DATA, fetchLoginData);
  yield takeEvery(HOME_FETCHING_DATA, fetchHomeData);
  yield takeEvery(CHAT_CONNECTION_OPEN, watchSocketConnection);
  yield takeEvery(CHAT_CONNECTION_CLOSE, watchSocketConnection);
  yield takeEvery(ADD_MESSAGE, watchSendMessage);
}
