import { createStackNavigator, createAppContainer } from 'react-navigation';
import {
  createNavigationReducer,
  reduxifyNavigator,
  createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import Login from './../src/screens/Login/Login';
import Home from './../src/screens/Home/Home';
import Chat from './../src/screens/Chat/Chat';
import PdfViewer from './../src/screens/PdfViewer/PdfViewer';
import ResponsiveUI from './screens/ResponsiveUI/ResponsiveUI';
import { Sliding } from './screens/Sliding/Sliding';
import { RouteNames } from './constants/navigation';
import { Launch } from './screens/Launch/Launch';
import { JanusWebRtc } from './screens/Video/JanusWebRtc';
import AppStateViewer from './screens/AppState/AppStateViewer';
import ToastViewer from './screens/Toast/ToastViewer';
import { ImageViewer } from './screens/Image/ImageViewer';
import { Browser } from './screens/Browser/Browser';
import { Portals } from './screens/Portals/Portals';
import { NativePortals } from './screens/Portals/NativePortals';
import { YouTubeViewer } from './screens/YouTube/YouTube';
import { CalendarViewer } from './screens/Calendar/Calendar';
import { LinearGradientViewer } from './screens/LinearGradient/LinearGradient';
import { NativePortalsDestination } from './screens/Portals/NativePortalsDestination';
import { ImmersiveViewer } from './screens/Immersive/Immersive';
import { ZIndexViewer } from './screens/ZIndex/ZIndex';
/*
export function configureNavigator(): any {
  return createStackNavigator(
    {
      Login: { screen: Login },
      Home: { screen: Home },
      Chat: { screen: Chat }
    },
    {
      initialRouteName: "Login"
    }
  );
}
*/
const PortalsModalStack = createStackNavigator(
  {
    MainPortals: {
      screen: NativePortals
    },
    ModalPortals: {
      screen: NativePortalsDestination
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
);

const RootNavigator = createStackNavigator(
  {
    [RouteNames.launch]: { screen: Launch },
    [RouteNames.appstate]: { screen: AppStateViewer },
    [RouteNames.login]: { screen: Login },
    [RouteNames.home]: { screen: Home },
    [RouteNames.chat]: { screen: Chat },
    [RouteNames.image]: { screen: ImageViewer },
    [RouteNames.immersive]: { screen: ImmersiveViewer },
    [RouteNames.browser]: { screen: Browser },
    [RouteNames.calendar]: { screen: CalendarViewer },
    [RouteNames.gradient]: { screen: LinearGradientViewer },
    [RouteNames.pdf]: { screen: PdfViewer },
    [RouteNames.portals]: { screen: Portals },
    [RouteNames.nPortals]: { screen: PortalsModalStack },
    [RouteNames.responsive]: { screen: ResponsiveUI },
    [RouteNames.sliding]: { screen: Sliding },
    [RouteNames.toast]: { screen: ToastViewer },
    [RouteNames.janus]: { screen: JanusWebRtc },
    [RouteNames.youtube]: { screen: YouTubeViewer },
    [RouteNames.zindex]: { screen: ZIndexViewer }
  },
  {
    initialRouteName: RouteNames.launch,
    headerMode: 'screen'
  }
);

const NavReducer = createNavigationReducer(RootNavigator);

const NavigatorMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state['nav']
);

const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');

const mapStateToProps = state => ({
  state: state.nav
});

// const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

const AppNavigator = createAppContainer(RootNavigator);

export { RootNavigator, NavReducer, AppNavigator, NavigatorMiddleware };
