import React from "react";
import { View, Platform } from "react-native";
import { Button } from "react-native-elements";
import {
    CustomTabs,
    ANIMATIONS_SLIDE,
    ANIMATIONS_FADE
} from "react-native-custom-tabs";
// import SafariView from "react-native-safari-view";

interface Props { }
export class Browser extends React.Component<Props> {
    private onOpenBrowserPress = () => {
        const googleUrl: string = "https://www.google.com";
        const glowblSignOutUrl: string = "https://account.glowbl.com/auth/signout";
        if (Platform.OS === "android") {
            this.openChromeCustomTabs(googleUrl);
        } else {
            this.openSafariView(googleUrl);
        }
    };

    private onaccounthomePress = () => {
        const glowblaccountUrl: string = "https://account.glowbl.com";
        if (Platform.OS === "android") {
            this.openChromeCustomTabs(glowblaccountUrl);
        } else {
            this.openSafariView(glowblaccountUrl);
        }
    };

    public openChromeCustomTabs = (url: string) => {
        CustomTabs.openURL(url)
            .then((launched: boolean) => {
                console.log(`Launched custom tabs: ${launched}`);
            })
            .catch(err => {
                console.error("An error occurred", err);
            });
    };

    public openSafariView(url: string) {
        /*
        SafariView.isAvailable()
            .then(
                SafariView.show({
                    url
                })
            )
            .catch(error => {
                // Fallback WebView code for iOS 8 and earlier
            });*/
    }

    public render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: "space-around",
                    alignItems: "center"
                }}
            >
                <Button title="Open browser" onPress={this.onOpenBrowserPress} />
            </View>
        );
    }
}
