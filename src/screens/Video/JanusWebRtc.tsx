import React, { ReactText, ReactElement } from "react";
import Janus from "./janus.mobile.js";
import {
  Alert,
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  DeviceEventEmitter
} from "react-native";
import { RTCView } from "react-native-webrtc";
import { IceServers } from "../../utils/janusUtils";
import InCallManager from "react-native-incall-manager";
import { Button, Icon } from "react-native-elements";
import Circle from "../../components/Circle";

let server: string = "wss://janus-1.backstage.cloud:5443";

let janus;
let sfutest;
let started: boolean = false;

let myusername: number = Math.floor(Math.random() * 1000);
let roomId: number = 8;
let myid = null;
let mystream = null;

let feeds = [];
var bitrateTimer = [];

Janus.init({
  debug: "all",
  callback: function() {
    if (started) return;
    started = true;
  }
});

interface State {
  info: string;
  status: string;
  isFront: boolean;
  selfViewSrc: any;
  selfViewSrcKey: any;
  remoteList: any;
  remoteListPluginHandle: any;
  publish: boolean;
  audioMute: boolean;
  videoMute: boolean;
  visible: boolean;
  videoWidth: number;
  videoHeight: number;
  tapCount: number;
  videoHided: boolean;
}

export class JanusWebRtc extends React.Component<{}, State> {
  private rtc1: RTCView;
  private rtc2: RTCView;

  constructor(props) {
    super(props);
    this.state = {
      info: "Initializing",
      status: "init",
      isFront: true,
      selfViewSrc: null,
      selfViewSrcKey: null,
      remoteList: {},
      remoteListPluginHandle: {},
      publish: false,
      audioMute: false,
      videoMute: false,
      visible: false,
      videoWidth: Dimensions.get("window").width,
      videoHeight: Dimensions.get("window").height / 2.35,
      tapCount: 0,
      videoHided: false
    };
  }

  componentDidMount() {
    DeviceEventEmitter.addListener("Proximity", this.onProximityEvent);
    DeviceEventEmitter.addListener("WiredHeadset", this.onWiredHeadsetEvent);
    DeviceEventEmitter.addListener("NoisyAudio", this.onNoisyAudioEvent);
    DeviceEventEmitter.addListener("MediaButton", this.onMediaButtonEvent);
    DeviceEventEmitter.addListener(
      "onAudioFocusChange",
      this.onAudioFocusChangeEvent
    );
    // --- start manager when the chat start based on logics of your app
    // On Call Established:
    InCallManager.start({ media: "video" }); // audio/video, default: audio
    InCallManager.setSpeakerphoneOn(true);
    // ... it will also register and emit events ...

    this.janusStart();
  }

  componentWillUnmount() {
    // --- On Call Hangup:
    InCallManager.stop();
    // ... it will also remote event listeners ...
  }

  private onProximityEvent = (data: { isNear: boolean }): void => {
    console.warn("onProximityEvent - isNear :", data.isNear);
  };
  private onSizeButtonPress = () => {
    console.log("on size button pressed");
    const { tapCount, videoWidth, videoHeight } = this.state;
    const factor = tapCount > 4 ? 2 : 0.5;
    this.setState({
      videoWidth: videoWidth * factor,
      videoHeight: videoHeight * factor,
      tapCount: tapCount + 1
    });
  };

  private onSwitchButtonPress = () => {
    console.log("on switch button pressed");
    this.setState({ videoHided: !this.state.videoHided });
  };

  private onWiredHeadsetEvent = (data: {
    isPlugged: boolean;
    hasMic: boolean;
    deviceName: string;
  }): void => {
    console.log(
      `onWiredHeadsetEvent - isPlugged :${data.isPlugged} - hasMic :${data.hasMic} - deviceName :${data.deviceName}`
    );
    if (!data.isPlugged) {
      InCallManager.setSpeakerphoneOn(true);
    }
  };

  private onNoisyAudioEvent = (): void => {
    console.log("onNoisyAudioEvent");
  };

  private onMediaButtonEvent = (data: {
    eventText: string;
    eventCode: number;
  }): void => {
    console.log(
      `onMediaButtonEvent - eventText :${data.eventText} - eventCode :${data.eventCode}`
    );
  };

  private onAudioFocusChangeEvent = (data: {
    eventText: string;
    eventCode: number;
  }): void => {
    console.log(
      `onAudioFocusChangeEvent - eventText :${data.eventText} - eventCode :${data.eventCode}`
    );
  };

  janusStart = () => {
    console.log("Janus - start");
    this.setState({ visible: true });
    janus = new Janus({
      server: server,
      iceServers: [IceServers.get()],
      success: () => {
        janus.attach({
          plugin: "janus.plugin.videoroom",
          success: pluginHandle => {
            console.log("Janus - on attach success", pluginHandle);
            sfutest = pluginHandle;
            /*
            // create room
            let create = {
              request: "create",
              room: roomId,
              permanent: true,
              is_private: false
            };
            sfutest.send({ message: create });
            */
            let register = {
              request: "join",
              room: roomId,
              ptype: "publisher",
              display: myusername.toString()
            };
            sfutest.send({ message: register });
          },
          error: error => {
            console.log("Janus - onError", error);
            Alert.alert("  -- Error attaching plugin...", error);
          },
          consentDialog: on => {
            console.log("Janus - onConsentDialog", on);
          },
          mediaState: (medium, on) => {
            console.log("Janus - onMediaState", medium, on);
          },
          webrtcState: on => {
            console.log("Janus - onWebRtcState", on);
          },
          onmessage: (msg, jsep) => {
            console.log("Janus - onMessage", msg, jsep);
            // console.log(msg)
            var event = msg["videoroom"];
            if (event != undefined && event != null) {
              if (event === "joined") {
                myid = msg["id"];
                this.publishOwnFeed(true);
                this.setState({ visible: false });
                if (
                  msg["publishers"] !== undefined &&
                  msg["publishers"] !== null
                ) {
                  var list = msg["publishers"];
                  for (var f in list) {
                    var id = list[f]["id"];
                    var display = list[f]["display"];
                    this.newRemoteFeed(id, display);
                  }
                }
              } else if (event === "destroyed") {
              } else if (event === "event") {
                if (
                  msg["publishers"] !== undefined &&
                  msg["publishers"] !== null
                ) {
                  var list = msg["publishers"];
                  for (var f in list) {
                    let id = list[f]["id"];
                    let display = list[f]["display"];
                    this.newRemoteFeed(id, display);
                  }
                } else if (
                  msg["leaving"] !== undefined &&
                  msg["leaving"] !== null
                ) {
                  var leaving = msg["leaving"];
                  var remoteFeed = null;
                  let numLeaving = parseInt(msg["leaving"]);
                  if (this.state.remoteList.hasOwnProperty(numLeaving)) {
                    delete this.state.remoteList.numLeaving;
                    this.setState({ remoteList: this.state.remoteList });
                    this.state.remoteListPluginHandle[numLeaving].detach();
                    delete this.state.remoteListPluginHandle.numLeaving;
                  }
                } else if (
                  msg["unpublished"] !== undefined &&
                  msg["unpublished"] !== null
                ) {
                  var unpublished = msg["unpublished"];
                  if (unpublished === "ok") {
                    sfutest.hangup();
                    return;
                  }
                  let numLeaving = parseInt(msg["unpublished"]);
                  if (this.state.remoteList.hasOwnProperty(numLeaving)) {
                    delete this.state.remoteList.numLeaving;
                    this.setState({ remoteList: this.state.remoteList });
                    this.state.remoteListPluginHandle[numLeaving].detach();
                    delete this.state.remoteListPluginHandle.numLeaving;
                  }
                } else if (
                  msg["error"] !== undefined &&
                  msg["error"] !== null
                ) {
                }
              }
            }
            if (jsep !== undefined && jsep !== null) {
              sfutest.handleRemoteJsep({ jsep: jsep });
            }
          },
          onlocalstream: stream => {
            console.log("Janus - onlocalstream", stream);
            this.setState({ selfViewSrc: stream.toURL() });
            this.setState({ selfViewSrcKey: Math.floor(Math.random() * 1000) });
            this.setState({
              status: "ready",
              info: "Please enter or create room ID"
            });
          },
          onremotestream: stream => {
            console.log("Janus - onremotestream", stream);
          },
          oncleanup: () => {
            mystream = null;
          }
        });
      },
      error: error => {
        Alert.alert("  Janus Error", error);
      },
      destroyed: () => {
        Alert.alert("  Success for End Call ");
        this.setState({ publish: false });
      }
    });
  };

  endCall = () => {
    janus.destroy();
  };

  publishOwnFeed = useAudio => {
    if (!this.state.publish) {
      console.log("start publish", useAudio);
      this.setState({ publish: true });
      sfutest.createOffer({
        media: {
          audioRecv: false,
          videoRecv: false,
          audioSend: useAudio,
          videoSend: true
        },
        success: jsep => {
          console.log("Janus - on offer success", jsep);
          var publish = {
            request: "configure",
            audio: useAudio,
            video: true,
            bitrate: 128000
          };
          // jsep = forceH264(jsep);
          sfutest.send({ message: publish, jsep: jsep });
        },
        error: error => {
          console.log("Janus - on offer error", error);
          Alert.alert("WebRTC error:", error);
          if (useAudio) {
            this.publishOwnFeed(false);
          } else {
          }
        }
      });
    } else {
      // this.setState({ publish: false });
      // let unpublish = { "request": "unpublish" };
      // sfutest.send({"message": unpublish});
    }
  };

  newRemoteFeed = (id, display) => {
    let remoteFeed;
    janus.attach({
      plugin: "janus.plugin.videoroom",
      success: pluginHandle => {
        console.log("Janus - on remote attach success", pluginHandle);
        remoteFeed = pluginHandle;
        let listen = {
          request: "join",
          room: roomId,
          ptype: "listener",
          feed: id
        };
        if (remoteFeed) {
          remoteFeed.send({ message: listen });
        }
      },
      error: error => {
        Alert.alert("  -- Error attaching plugin...", error);
      },
      onmessage: (msg, jsep) => {
        console.log("Janus - on remote message", msg, jsep);
        let event = msg["videoroom"];
        if (event != undefined && event != null) {
          if (event === "attached") {
            // Subscriber created and attached
          }
        }
        if (jsep !== undefined && jsep !== null) {
          remoteFeed.createAnswer({
            jsep: jsep,
            media: { audioSend: false, videoSend: false },
            success: jsep => {
              console.log("Janus - on remoteFeed createAnswer success", jsep);
              var body = { request: "start", room: roomId };
              remoteFeed.send({ message: body, jsep: jsep });
            },
            error: error => {
              console.log("Janus - on remoteFeed createAnswer error", error);
              Alert.alert("WebRTC error:", error);
            }
          });
        }
      },
      webrtcState: on => {
        console.log("Janus - on webrtc state", on);
      },
      onlocalstream: stream => {
        console.log("Janus - on Local Stream", stream);
      },
      onremotestream: stream => {
        console.log("Janus - on Remote Stream", stream);
        this.setState({ info: "One peer join!" });
        const remoteList = this.state.remoteList;
        const remoteListPluginHandle = this.state.remoteListPluginHandle;
        remoteList[id] = stream.toURL();
        remoteListPluginHandle[id] = remoteFeed;
        this.setState({
          remoteList: remoteList,
          remoteListPluginHandle: remoteListPluginHandle
        });
      },
      oncleanup: () => {
        console.log("Janus - on Cleanup");
        if (remoteFeed.spinner !== undefined && remoteFeed.spinner !== null)
          remoteFeed.spinner.stop();
        remoteFeed.spinner = null;
        if (
          bitrateTimer[remoteFeed.rfindex] !== null &&
          bitrateTimer[remoteFeed.rfindex] !== null
        )
          clearInterval(bitrateTimer[remoteFeed.rfindex]);
        bitrateTimer[remoteFeed.rfindex] = null;
      }
    });
  };

  render() {
    console.log("render", this.state);
    return (
      <React.Fragment>
        <ScrollView>
          <View style={styles.container}>
            {this.state.selfViewSrc && !this.state.videoHided && (
              <Circle
                radius={this.state.videoWidth * 0.5}
                backgroundColor={"white"}
              >
                <RTCView
                  ref={rtc => {
                    this.rtc1 = rtc;
                  }}
                  key={this.state.selfViewSrcKey}
                  streamURL={this.state.selfViewSrc}
                  objectFit="cover"
                  style={[
                    styles.remoteView,
                    {
                      width: this.state.videoWidth,
                      height: this.state.videoHeight
                    }
                  ]}
                />
              </Circle>
            )}
            {this.state.remoteList &&
              !this.state.videoHided &&
              Object.keys(this.state.remoteList).map((key, index) => {
                return (
                  <Circle
                    radius={this.state.videoWidth * 0.5}
                    backgroundColor={"white"}
                  >
                    <RTCView
                      ref={rtc => {
                        this.rtc2 = rtc;
                      }}
                      key={Math.floor(Math.random() * 1000)}
                      streamURL={this.state.remoteList[key]}
                      objectFit="cover"
                      style={[
                        styles.remoteView,
                        {
                          width: this.state.videoWidth,
                          height: this.state.videoHeight
                        }
                      ]}
                    />
                  </Circle>
                );
              })}
          </View>
        </ScrollView>

        <Button
          type="clear"
          icon={
            <Icon
              reverse
              name="loupe"
              type="material"
              size={15}
              color="blue"
              onPress={this.onSizeButtonPress}
            />
          }
          containerStyle={{ position: "absolute", bottom: 5, right: 5 }}
        />
        <Button
          type="clear"
          icon={
            <Icon
              reverse
              name="loop"
              type="material"
              size={15}
              color="blue"
              onPress={this.onSwitchButtonPress}
            />
          }
          containerStyle={{ position: "absolute", bottom: 50, right: 5 }}
        />
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  selfView: {
    width: 200,
    height: 150
  },
  remoteView: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 2.35,
    backgroundColor: "black",
    borderRadius: 20
  },
  container: {
    flex: 1,
    borderRadius: 20,
    justifyContent: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listViewContainer: {
    height: 150
  }
});
