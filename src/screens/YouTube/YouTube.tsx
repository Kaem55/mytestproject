import React from "react";
import { View, PixelRatio, StyleSheet, ScrollView } from "react-native";
import YouTube, {
  YouTubeStandaloneIOS,
  YouTubeStandaloneAndroid
} from "react-native-youtube";
import { Button } from "react-native-elements";

interface IState {
  isReady: boolean;
  status: any;
  quality: any;
  error: any;
  isPlaying: boolean;
  isLooping: boolean;
  duration: number;
  currentTime: number;
  isFullScreen: boolean;
  containerMounted: boolean;
  containerWidth: number;
}

interface IProps {}

export class YouTubeViewer extends React.Component<IProps, IState> {
  private apiKey: string = "AIzaSyAr_DNlR7k4zzqS5_F8wcHAcHVEady9d7M";
  private _player: any;

  public constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      status: null,
      quality: null,
      error: null,
      isPlaying: true,
      isLooping: true,
      duration: 0,
      currentTime: 0,
      isFullScreen: false,
      containerMounted: false,
      containerWidth: 0
    };
  }

  private getYouTubeRef = (ref: any) => {
    this._player = ref;
  };

  private onFullScreenButtonPress = () => {
    const { isFullScreen } = this.state;
    this.setState({ isFullScreen: isFullScreen ? false : true });
  };

  private onError = error => {};

  public render(): JSX.Element {
    console.log("Youtube - render", this.state);

    const { isFullScreen } = this.state;
    return (
      <View
        style={styles.container}
        onLayout={({
          nativeEvent: {
            layout: { width }
          }
        }) => {
          if (!this.state.containerMounted)
            this.setState({ containerMounted: true });
          if (this.state.containerWidth !== width)
            this.setState({ containerWidth: width });
        }}
      >
        {this.state.containerMounted && (
          <YouTube
            ref={this.getYouTubeRef}
            style={[
              {
                height: PixelRatio.roundToNearestPixel(
                  this.state.containerWidth / (16 / 9)
                ),
                width: this.state.containerWidth
              },
              styles.player
            ]}
            apiKey={this.apiKey}
            videoId="G1IbRujko-A" // The YouTube video ID
            fullscreen={isFullScreen} // control whether the video should play in fullscreen or inline
            controls={1}
            rel={false}
            modestbranding={true}
            showinfo={true}
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onError={e => this.setState({ error: e.error })}
          />
        )}

        <Button
          title={"set FullScreen " + (isFullScreen ? "false" : "true")}
          onPress={this.onFullScreenButtonPress}
          style={{ alignSelf: "center" }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  buttonGroup: {
    flexDirection: "row",
    alignSelf: "center"
  },
  button: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    alignSelf: "center"
  },
  buttonText: {
    fontSize: 18,
    color: "blue"
  },
  buttonTextSmall: {
    fontSize: 15
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  player: {
    alignSelf: "stretch",
    marginVertical: 10
  }
});
