import React from "react";
import {
  FlatList,
  ActivityIndicator,
  View,
  Text,
  Button,
  Alert
} from "react-native";
import { Icon } from "react-native-elements";
import { ListItem } from "react-native-elements";
import styles from "./homeStyles";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch, AnyAction } from "redux";
//import * as Actions from "../../actions/homeActions";
import { fetchHomeData, openConnection } from "../../actions/homeActions";
import Reactotron from "reactotron-react-native";
import { Dbg } from "../../utils/dbgUtils";

interface BaseProps {
  navigation: any;
}
interface StateFromProps {
  dataSource: any[];
  isLoading: boolean;
}

interface DispatchFromProps {
  fetchList: () => void;
  openConnection: () => void;
}
type Props = BaseProps & StateFromProps & DispatchFromProps;

export interface State {}
class Home extends React.Component<Props, State> {
  static navigationOptions = {
    title: "Home",
    headerStyle: {
      backgroundColor: "#9f85d0"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    },
    headerRight: (
      <Icon
        name="g-translate"
        color="#00aced"
        onPress={() => Alert.alert("This is a button!")}
      />
    )
  };

  constructor(props) {
    super(props);
    Dbg.debug("Show Home screen");
    this.handleItemTrigger = this.handleItemTrigger.bind(this);
  }

  private handleItemTrigger(item: any): void {
    Dbg.warn("Go to chat*");
    //this.props.openConnection();
    this.props.navigation.navigate("Chat", {
      name: item.name,
      avatar: item.avatar
    });
  }

  componentDidMount() {
    this.props.fetchList();
  }

  keyExtractor = (item, index) => index + "";

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      subtitle={"user"}
      leftAvatar={{ rounded: true, source: { uri: item.avatar } }}
      onPress={() => this.handleItemTrigger(item)}
    />
  );

  render() {
    console.log("home render isLoading:", this.props.isLoading);
    if (this.props.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            padding: 20,
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.dataSource}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: any): StateFromProps => ({
  dataSource: state.homeReducer.data,
  isLoading: state.homeReducer.isLoading
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchFromProps => ({
  fetchList: () => dispatch(fetchHomeData()),
  openConnection: () => dispatch(openConnection())
});

/*
function mapStateToProps(state: any, props: any) {
  return {
    dataSource: state.homeReducer.data,
    isLoading: state.homeReducer.isLoading
  };
}

function mapDispatchToProps(dispatch: Dispatch<AnyAction>) {
  return { fetchList: () => dispatch(Actions.fetchHomeData()) };
}
*/
/*
function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    fetchList: () => dispatch(fetchHomeData())
  };
}
*/
export default connect<StateFromProps, DispatchFromProps>(
  mapStateToProps,
  mapDispatchToProps
)(Home);
