import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: "#FBFAFA"
  },
  row: {
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  mt: {
    marginTop: 18
  }
});
export default styles;
