import React from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./loginStyles";
import { fetchLoginData, resetData } from "../../actions/loginActions";
import { bindActionCreators, Dispatch, AnyAction } from "redux";
import { connect } from "react-redux";
import {
  ResponsiveComponent,
  ResponsiveStyleSheet
} from "react-native-responsive-ui";

export interface IProps {
  navigation: any;
  fetchData(): void;
  resetData(): void;
  isLoading: Boolean;
  data: String;
}
export interface IState {}
class Login extends React.Component<IProps, IState> {
  static navigationOptions = {
    title: "Login",
    headerStyle: {
      backgroundColor: "#9f85d0"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };
  /*
  static navigationOptions = ({ navigation }) => {
    return { title: navigation.getParam("name", "") };
  };
  */
  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    console.log("Login render");
    return (
      <View style={styles.container}>
        <Text>{this.props.data}</Text>
        <Button
          style={styles.button}
          icon={<Icon name="arrow-right" size={15} color="white" />}
          title="Next"
          onPress={this.handleNextPress}
        />
        <Button
          style={styles.button}
          title="Reset"
          onPress={this.handleResetPress}
        />
      </View>
    );
  }

  private handleNextPress = () => {
    this.props.navigation.navigate("Home");
  };

  private handleResetPress = () => {
    this.props.resetData();
  };
}

/*
  function mapStateToProps(state: any) {
    return {
      data: state.data,
      isLoading: state.isLoading
    };
  }

  function mapDispatchToProps(dispatch: Dispatch) {
    return { fetchData: () => dispatch(fetchLoginData()) };
  }
  */

const mapStateToProps = (state: any): any => ({
  data: state.loginReducer.data,
  isLoading: state.loginReducer.isLoading
});

const mapDispatchToProps = (dispatch: Dispatch): any => ({
  fetchData: () => dispatch(fetchLoginData()),
  resetData: () => dispatch(resetData())
});

/*
function mapStateToProps(state: any, props: any) {
  return {
    data: state.data,
    isLoading: state.isLoading
  };
}

function mapDispatchToProps(dispatch: Dispatch<AnyAction>) {
  return bindActionCreators(Actions, dispatch);
}
*/
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
