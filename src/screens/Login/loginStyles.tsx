import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20,
    paddingTop: 22,
    backgroundColor: "#FBFAFA"
  },
  button: {
    paddingTop: 20,
    marginBottom: 10
  },
  row: {
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  mt: {
    marginTop: 18
  }
});
export default styles;
