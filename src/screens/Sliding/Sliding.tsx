import React, { ReactText } from "react";
import {
  View,
  Button,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Animated,
  LayoutChangeEvent,
  TextInput,
  EmitterSubscription,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import SlidingUpPanel from "rn-sliding-up-panel";
import { colors } from "react-native-elements";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.grey0
  },
  containerA: {
    flex: 2,
    backgroundColor: "#ffe111"
  },
  containerB: {
    flex: 4,
    backgroundColor: "#245631",
    alignItems: "center",
    justifyContent: "center"
  },
  containerC: {
    flex: 1,
    backgroundColor: "#ff7511",
    alignItems: "center"
  },
  dragHandler: {
    alignSelf: "stretch",
    height: 64,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ccc"
  }
});

interface State {
  draggableRange: { top: number; bottom: number };
  panel: string;
  animatedId: string;
  isKeyboardOpen: boolean;
  containerAMounted: boolean;
}

export class Sliding extends React.Component<{}, State> {
  private _panel: any;
  private _animatedValue: Animated.Value;
  private _listeners: Array<EmitterSubscription> = [];

  constructor(props) {
    super(props);
    this._animatedValue = new Animated.Value(0);
    this.state = {
      draggableRange: {
        top: Dimensions.get("window").height - 250,
        bottom: 250
      },
      panel: "down",
      animatedId: "",
      isKeyboardOpen: false,
      containerAMounted: true
    };
  }

  componentDidMount() {
    this._listeners = [
      Keyboard.addListener("keyboardDidShow", this.onKeyboardShowed),
      Keyboard.addListener("keyboardDidHide", this.onKeyboardHided)
    ];
    const id: string = this._animatedValue.addListener(
      this._onAnimatedValueChange
    );
    this.setState({ animatedId: id });
  }

  componentWillUnmount() {
    this._listeners.forEach(listener => listener.remove());
    this._animatedValue.removeListener(this.state.animatedId);
  }

  _onAnimatedValueChange = ({ value }) => {
    console.log("_onAnimatedValueChange", value);
    const { top, bottom } = this.state.draggableRange;
    if (value === top) {
      this.setState({ panel: "up" });
    }
    if (value === bottom) {
      this.setState({ panel: "down" });
    }
  };

  onKeyboardShowed = e => {
    console.log("onKeyboardShowed");
    this.setState({ isKeyboardOpen: true });
    this._panel.show();
  };

  onKeyboardHided = () => {
    console.log("onKeyboardHided");
    this.setState({ isKeyboardOpen: false });
  };

  onHeaderTriggered = () => {
    console.log("onHeaderTriggered", this._panel.props.height, this.state);
    if (this.state.panel == "down") {
      this._panel.show();
    } else this._panel.hide();
  };

  onContainerA_dimensionsChanged = (event: LayoutChangeEvent) => {
    console.log("onContainerA_dimensionsChanged", event.nativeEvent.layout);
    this.setState({
      draggableRange: {
        top: Dimensions.get("window").height - event.nativeEvent.layout.height,
        bottom: 250
      },
      containerAMounted: true
    });
  };

  render() {
    console.log("render state", this.state);
    const isLandscape: boolean =
      Dimensions.get("window").width > Dimensions.get("window").height;
    return (
      <KeyboardAvoidingView style={styles.container} keyboardVerticalOffset={0}>
        <View
          style={styles.containerA}
          onLayout={this.onContainerA_dimensionsChanged}
        />
        <View style={styles.containerB}>
          <Button
            title="Show panel"
            onPress={() => {
              this._panel.show();
            }}
          />
        </View>
        <SlidingUpPanel
          ref={c => (this._panel = c)}
          draggableRange={{
            top: this.state.draggableRange.top,
            bottom: this.state.draggableRange.bottom
          }}
          height={this.state.draggableRange.top}
          animatedValue={this._animatedValue}
          allowDragging={false}
          showBackdrop={false}
        >
          <View
            style={[
              styles.container,
              {
                width: isLandscape
                  ? Dimensions.get("window").width * 0.5
                  : Dimensions.get("window").width,
                alignSelf: "flex-end"
              }
            ]}
          >
            <View
              style={[
                styles.dragHandler,
                { height: this.state.panel === "down" ? 64 : 30 }
              ]}
              onTouchEnd={this.onHeaderTriggered}
            >
              <Text>Drag handler</Text>
            </View>
            <ScrollView>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
              <Text>Here is the content inside panel</Text>
            </ScrollView>
          </View>
        </SlidingUpPanel>
        <View style={styles.containerC}>
          <TextInput
            style={{ backgroundColor: "white", height: "80%", width: "80%" }}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}
