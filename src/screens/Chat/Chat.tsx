import React from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
  Image
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch, AnyAction } from "redux";
import styles from "./chatStyles";
import * as actions from "../../actions/chatActions";

export interface StateFromProps {
  messages: any[];
  typing: string;
}

export interface DispatchFromProps {
  openConnection: Function;
  closeConnection: Function;
  addMessage(dd, ff, hh): void;
  typingChanged: Function;
}

export interface BaseProps {
  navigation: any;
}

type Props = BaseProps & DispatchFromProps & StateFromProps;

export interface State {}
class Chat extends React.Component<Props, State> {
  static navigationOptions = {
    title: "Chat",
    headerStyle: {
      backgroundColor: "#9f85d0"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };

  private name!: string;
  private avatar!: string;
  private textInput!: any;

  constructor(props) {
    super(props);
    console.log("chat - constructor");
    this.renderListView = this.renderListView.bind(this);
    this.prepareDataSources = this.prepareDataSources.bind(this);
    this.onSending = this.onSending.bind(this);
    this.onTextFieldChange = this.onTextFieldChange.bind(this);
    this.handleSendingMessage = this.handleSendingMessage.bind(this);
    this.handleChangeText = this.handleChangeText.bind(this);

    const { navigation } = this.props;
    this.name = navigation.getParam("name");
    this.avatar = navigation.getParam("avatar");
  }

  componentDidMount() {
    console.log("chat - componentDidMount");
    this.props.openConnection();
  }

  componentWillUnmount() {
    console.log("chat - componentWillUnmount");
    this.props.closeConnection();
  }

  keyExtractor = (item, index) => index + "";

  renderItem({ item }) {
    return (
      <View style={styles.row}>
        <Image style={styles.avatar} source={{ uri: item.avatar }} />
        <View style={styles.rowText}>
          <Text style={styles.sender}>{item.author}</Text>
          <Text style={styles.message}>{item.message}</Text>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.messages}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.footer}>
            <TextInput
              ref={input => {
                this.textInput = input;
              }}
              value={this.props.typing}
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Type something here..."
              onChangeText={text => this.handleChangeText(text)}
            />
            <TouchableOpacity onPress={this.handleSendingMessage}>
              <Text style={styles.send}>Send</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }

  renderListView() {}

  prepareDataSources() {}

  onSending() {}

  onTextFieldChange() {}

  handleSendingMessage() {
    console.log("send message : ", this.props.typing, this.name, this.avatar);
    this.props.addMessage(this.props.typing, this.name, this.avatar);
  }

  handleChangeText(text: string) {
    this.props.typingChanged(text);
  }
}

const mapStateToProps = (state: any): StateFromProps => ({
  messages: state.chatMessagesReducer.messages,
  typing: state.chatMessagesReducer.typing
});

/*
const mapDispatchToProps = (dispatch: Dispatch): DispatchFromProps => ({
  
  sendMessage: (message: string, author: string) =>
    dispatch(actions.addMessage(message, author)),
  openConnection: () =>

});
*/

function mapDispatchToProps(dispatch: Dispatch): DispatchFromProps {
  return bindActionCreators(
    {
      ...actions
    },
    dispatch
  );
}

export default connect<StateFromProps, DispatchFromProps>(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
