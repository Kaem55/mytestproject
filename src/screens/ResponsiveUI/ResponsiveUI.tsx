import React from "react";
import {
  MediaQuery,
  ResponsiveComponent,
  ResponsiveStyleSheet
} from "react-native-responsive-ui";
import {
  View,
  StyleSheet,
  ViewStyle,
  Text,
  StatusBar,
  YellowBox
} from "react-native";
import ResponsiveList from "../../components/RecyclerList/ResponsiveList";

interface Style {
  mainContainer: ViewStyle;
  btn: ViewStyle;
  containerB: ViewStyle;
  containerC: ViewStyle;
}

interface Props {
  navigation: any;
}

export default class ResponsiveUI extends ResponsiveComponent<Props> {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      title: "ResponsiveUI",
      headerStyle: {
        backgroundColor: "white"
      },
      header: navigation.state.params
        ? navigation.state.params.header
        : undefined
    };
  };
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
      "Warning: isMounted(...) is deprecated",
      "Module RCTImageLoader"
    ]);
  }

  public componentDidUpdate(prevProps, prevState, snapshot) {
    let dimensionChanged: boolean = prevState.window !== this.state.window;
    const isLandscape: boolean =
      this.state.window.width > this.state.window.height;
    console.log(
      "componentDidUpdate dimensionChanged isLandscape",
      dimensionChanged,
      isLandscape
    );
    if (dimensionChanged) {
      this.props.navigation.setParams({
        header: isLandscape ? null : undefined
      });
    }
  }

  public render() {
    const style = this.getStyle();
    const isLandscape: boolean =
      this.state.window.width > this.state.window.height;
    return (
      <View style={style.mainContainer}>
        <MediaQuery orientation="landscape">
          <StatusBar hidden={true} />
        </MediaQuery>
        <View style={mainStyle.containerA} />
        <ResponsiveList
          isLandscape={isLandscape}
          manageLandscape={false}
          logsEnabled={true}
          style={[mainStyle.containerB, style.containerB]}
        />
        {/*<View style={[mainStyle.containerB, style.containerB]} />*/}
        <ResponsiveList
          isLandscape={isLandscape}
          manageLandscape={true}
          style={[mainStyle.containerC, style.containerC]}
          lenght={6}
        />
        {/*<View style={[mainStyle.containerC, style.containerC]} />*/}
        <MediaQuery orientation="portrait">
          <View style={mainStyle.containerD} />
        </MediaQuery>
      </View>
    );
  }

  getStyle(): StyleSheet.NamedStyles<Style> {
    return ResponsiveStyleSheet.select([
      {
        query: { orientation: "landscape" },
        style: {
          mainContainer: {
            flex: 1,
            flexDirection: "row"
          },
          containerB: {
            flex: 3
          },
          containerC: {
            flex: 1
          },
          btn: {
            flex: 1
          }
        }
      },
      {
        query: { orientation: "portrait" },
        style: {
          mainContainer: {
            flex: 1,
            flexDirection: "column"
          },
          containerC: {
            flex: 3
          },
          btn: {
            flex: 0
          }
        }
      }
    ]);
  }
}

const mainStyle = StyleSheet.create({
  containerA: {
    flex: 1,
    backgroundColor: "rgba(80, 210, 194, .8)",
    justifyContent: "center",
    alignItems: "center"
  },
  containerB: {
    flex: 2,
    backgroundColor: "rgba(88, 100, 274, .8)"
  },
  containerC: {
    flex: 3,
    backgroundColor: "rgba(123, 120, 140, 5)"
  },
  containerD: {
    flex: 1,
    backgroundColor: "rgba(256, 16, 16, .8)",
    justifyContent: "center",
    alignItems: "center"
  }
});
