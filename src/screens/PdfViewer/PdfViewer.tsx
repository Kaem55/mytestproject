import React from "react";
import {
  StyleSheet,
  TouchableHighlight,
  Dimensions,
  View,
  Text
} from "react-native";
import { bindActionCreators, Dispatch, AnyAction } from "redux";
import { connect } from "react-redux";
import Pdf from "react-native-pdf";
import styles from "./PdfViewerStyles";
import * as actions from "../../actions/pdfActions";
export interface IBaseProps {
  navigation: any;
}
export interface IStateFromProps {
  isLoading: Boolean;
  page: number;
  scale: number;
  numberOfPages: number;
  horizontal: boolean;
}
export interface IDispatchFromProps {
  zoomChanged(zoom: number): void;
  scrollChanged(scroll: number): void;
}
type IProps = IBaseProps & IStateFromProps & IDispatchFromProps;
export interface IState { }
class PdfViewer extends React.Component<IProps, IState> {
  private pdf: any;

  constructor(props) {
    super(props);
    this.handleZoom = this.handleZoom.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    /*
    this.state = {
      page: 1,
      scale: 1,
      numberOfPages: 0,
      horizontal: false
    };*/
    this.pdf = null;
  }

  componentDidMount() { }

  handleZoom(text: string) {
    //this.props.typingChanged(text);
  }

  handleScroll(): void { }

  render() {
    console.log("PdfViewer - render");
    //let source = {
    // uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
    // cache: false
    //};
    //let source = require('./test.pdf');  // ios only
    let source = {
      uri:
        "https://www.leshouches.com/img/sitra/fiche/820526-tmb-fiche-smbt.cd74.pdf",
      cache: false
    };

    // let source = { uri: "bundle-assets://test.pdf" };

    //let source = { uri: "file:///sdcard/test.pdf" };
    //let source = { uri: "" };

    return (
      <View style={styles.container}>
        <Pdf
          ref={pdf => {
            this.pdf = pdf;
          }}
          source={source}
          //page={this.props.page}
          //scale={this.props.scale}
          //horizontal={this.props.horizontal}
          enablePaging={false}
          onLoadComplete={(numberOfPages, filePath) => {
            //this.props.numberOfPages = numberOfPages; //do not use setState, it will cause re-render
            console.log(`total page count: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            //this.props.page = page; //do not use setState, it will cause re-render
            console.log(`current page: ${page}`);
          }}
          onError={error => {
            console.log(error);
          }}
          onPageSingleTap={(page: number) => {
            console.log(page);
          }}
          style={styles.pdf}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  isLoading: state.pdfReducer.isLoading,
  page: state.pdfReducer.page,
  scale: state.pdfReducer.scale,
  numberOfPages: state.pdfReducer.numberOfPages,
  horizontal: state.pdfReducer.horizontal
});

function mapDispatchToProps(dispatch: Dispatch): any {
  return bindActionCreators(
    {
      ...actions
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PdfViewer);
