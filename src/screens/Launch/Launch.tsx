import React from "react";
import { ListItem } from "react-native-elements";
import { View, ActivityIndicator, FlatList } from "react-native";
import styles from "./LaunchStyles";
import { buildScreensArray } from "../../utils/screens";

interface Props {
  navigation: any;
}

interface State {
  dataSource: Array<string>;
}

const screens: Array<any> = buildScreensArray();

export class Launch extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      dataSource: screens
    };
  }

  handleItemTrigger(item: any): void {
    console.log("item triggered", item);
    this.props.navigation.navigate(item.id);
  }

  keyExtractor = (item, index) => index + "";

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      titleStyle={styles.text}
      onPress={() => this.handleItemTrigger(item)}
    />
  );

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataSource}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}
