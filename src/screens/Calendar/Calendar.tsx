import React from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
// import Calendar from "react-native-calendar";

interface Props {}
interface State {}

export class CalendarViewer extends React.Component<Props, State> {
  private onSendPress = () => {
    console.info("send calendar event");

    /*Calendar.addEvent(
      "Birthday Party",
      "4 Privet Drive, Surrey",
      new Date().getTime()
    );*/

    /*
    Calendar.addEvent("Birthday Party", {
      location: "4 Privet Drive, Surrey",
      time: new Date().getTime(),
      description: "..."
    });*/
  };

  private onSendCallbackPress = () => {
    /*Calendar.getStatus((error: any, isOn: boolean) => {
      console.log("callback return - error:" + error + " isOn:" + isOn);
    });*/
  };

  public render(): JSX.Element {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Button title="send Event" onPress={this.onSendPress} />
        <Button
          style={{ paddingTop: 10 }}
          title="send callback"
          onPress={this.onSendCallbackPress}
        />
      </View>
    );
  }
}
