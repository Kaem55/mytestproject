import React from 'react';
import { View, Alert } from 'react-native';
import { Immersive } from 'react-native-immersive';
import { Button, Divider } from 'react-native-elements';

interface State {}
export class ImmersiveViewer extends React.Component<State> {
  public componentDidMount(): void {
    Immersive.addImmersiveListener(this.restoreImmersive);
    Immersive.removeImmersiveListener(this.restoreImmersive);
  }

  public render(): JSX.Element {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'black'
        }}
      >
        <Button
          title="On"
          style={{ padding: 20 }}
          onPress={() => {
            Immersive.on();
            Immersive.setImmersive(true);
          }}
        />
        <Divider style={{ height: 20 }} />
        <Button
          title="Off"
          style={{ padding: 20 }}
          onPress={() => {
            Immersive.off();
            Immersive.setImmersive(false);
          }}
        />
        <Divider style={{ height: 20 }} />
        <Button
          title="Show alert"
          style={{ padding: 20 }}
          onPress={() => {
            Alert.alert('help', 'this is a modal');
          }}
        />
      </View>
    );
  }

  // listener for Immersive State recover
  private restoreImmersive = () => {
    console.warn('Immersive State Changed!');
    Immersive.on();
    Immersive.setImmersive(true);
  };
}
