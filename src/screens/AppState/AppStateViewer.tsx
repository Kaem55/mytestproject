import React from "react";
import { View, Text, AppState, Platform } from "react-native";
import BackgroundTimer from "../../utils/timerUtils";
import { connect } from "react-redux";
import { startTimer, stopTimer } from "../../actions/timerActions";
import { Dispatch } from "redux";

interface IState {
  appState: string;
  counter: number;
  lastDate: Date;
}

interface IDispatchFromProps {
  startTimer: (delay: number) => void;
  stopTimer: () => void;
}

class AppStateViewer extends React.Component<IDispatchFromProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      counter: 0,
      lastDate: new Date()
    };
  }

  componentDidMount() {
    // AppState.addEventListener("change", this._handleAppStateChange);
    // setInterval(this.handleTimerEvent, 5000);
    this.props.startTimer(1000);
  }

  componentWillUnmount() {
    // AppState.removeEventListener("change", this._handleAppStateChange);
    this.props.stopTimer();
  }

  private tickNumber: number = 0;
  private handleTimerEvent = () => {
    const date = new Date();
    const { lastDate } = this.state;
    const diff = date.getTime() - lastDate.getTime();
    console.log("timer event " + this.tickNumber++, date);
    console.log(
      "previous time :" +
        this.state.lastDate.getTime() +
        " current time :" +
        date.getTime() +
        " diff :" +
        diff
    );
    this.setState({ counter: this.state.counter + 1, lastDate: date });
  };

  private _handleAppStateChange = nextAppState => {
    console.log("New app state :", nextAppState);
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      this.stopBackgroundTimer();
    }
    if (
      this.state.appState.match(/active/) &&
      (nextAppState === "inactive" || nextAppState === "background")
    ) {
      console.log("App come inactive!");
      this.startBackgroundTimer();
    }
    this.setState({ appState: nextAppState });
  };

  private intervalId;
  private startBackgroundTimer = (): void => {
    this.intervalId = BackgroundTimer.setInterval(this.handleTimerEvent, 5000);
    /*
    if (Platform.OS === "ios") {
      BackgroundTimer.start();
    } else if (Platform.OS === "android") {
      // Start a timer that runs continuous after X milliseconds
      this.intervalId = BackgroundTimer.setInterval(
        this.handleTimerEvent,
        5000
      );
    }*/
  };

  private stopBackgroundTimer = (): void => {
    BackgroundTimer.clearInterval(this.intervalId);
    /*if (Platform.OS === "ios") {
      BackgroundTimer.stop();
    } else if (Platform.OS === "android") {
      // Cancel the timer when you are done with it
      BackgroundTimer.clearInterval(this.intervalId);
    }*/
  };

  public render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <View style={{ flexDirection: "row", paddingBottom: 10 }}>
          <Text>State : </Text>
          <Text>{this.state.appState}</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text>Counter : </Text>
          <Text>{this.state.counter}</Text>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  startTimer: delay => dispatch(startTimer(delay)),
  stopTimer: () => dispatch(stopTimer())
});

export default connect<{}, IDispatchFromProps>(
  null,
  mapDispatchToProps
)(AppStateViewer);
