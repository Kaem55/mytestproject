import React from "react";
import {
  View,
  TouchableOpacity,
  TextComponent,
  findNodeHandle
} from "react-native";
import { Button, Text } from "react-native-elements";
import PortalProvider from "../../components/Portals/PortalProvider";
import PortalGate from "../../components/Portals/PortalGate";

interface Props {}
interface State {
  currentBlock: String;
}
export class Portals extends React.Component<Props, State> {
  private _text: any;

  constructor(props) {
    super(props);
    this.state = { currentBlock: "blue" };
  }

  private PortalText: JSX.Element = (
    <Text style={{ color: "white" }}>Hello</Text>
  );
  private getTextRef = (ref: any) => {
    this._text = findNodeHandle(ref);
  };

  private onButtonPress = () => {};

  private getNextBlockName = (): string => {
    const { currentBlock } = this.state;
    return currentBlock === "blue" ? "red" : "blue";
  };

  private setNextBlockState = (): void => {
    const { currentBlock } = this.state;
    this.setState({ currentBlock: currentBlock === "blue" ? "red" : "blue" });
  };

  public render() {
    // const pText: JSX.Element = <Text style={{ color: 'white' }}>Hello</Text>

    return (
      <PortalProvider>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View
              style={{
                flex: 1,
                backgroundColor: "blue",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <PortalGate gateName="blue" />
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: "red",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <PortalGate gateName="red" />
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <PortalGate gateName="touch">
              {teleportFN => (
                <Button
                  title="Switch"
                  onPress={() => {
                    teleportFN(this.getNextBlockName(), this.PortalText);
                    this.setNextBlockState();
                  }}
                />
              )}
            </PortalGate>
          </View>
        </View>
      </PortalProvider>
    );
  }
}
