import React from 'react';
import { View, Button, Dimensions, ScaledSize } from 'react-native';
import { PortalDestination, PortalOrigin } from 'rn-native-portals';
import DeviceInfo from 'react-native-device-info';
import { NavigationScreenProp } from 'react-navigation';

interface Props {
  navigation: NavigationScreenProp<any, any>;
}
interface State {
  window: ScaledSize;
  containerMounted: boolean;
  containerWidth: number;
}

export class NativePortalsDestination extends React.Component<Props, State> {
  static navigationOptions = {
    header: null
  };
  /*
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: (
        <Button onPress={() => navigation.goBack()} title="Info" color="#fff" />
      )
    };
  };
*/
  public constructor(props) {
    super(props);
    this.state = {
      window: Dimensions.get('window'),
      containerMounted: false,
      containerWidth: 0
    };
  }

  public componentDidMount() {
    Dimensions.addEventListener('change', this.onChange);
    /*this.subscription = Device.subscribeToDimensionChanges(dims =>
          this.setState(dims)
        );*/
  }

  public componentWillUnmount() {
    Dimensions.removeEventListener('change', this.onChange);
    // this.subscription.unsubscribe();
  }

  public componentDidUpdate(prevProps: Props, prevState: State) {
    const isLandscape: boolean = DeviceInfo.isLandscape();
    if (!isLandscape) {
      this.props.navigation.goBack();
    }
  }

  public render(): JSX.Element {
    const isLandscape: boolean = DeviceInfo.isLandscape();
    console.log('NativeModalPortals render - isLandscape: ', isLandscape);

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'black'
        }}
        onLayout={({
          nativeEvent: {
            layout: { width }
          }
        }) => {
          if (!this.state.containerMounted)
            this.setState({ containerMounted: true });
          if (this.state.containerWidth !== width)
            this.setState({ containerWidth: width });
        }}
      >
        <PortalDestination name="targetOfTeleportation" />
      </View>
    );
  }

  private onChange = (dims: { window: ScaledSize }) => this.setState(dims);
}
