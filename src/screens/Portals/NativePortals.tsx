import React from "react";
import {
  View,
  TouchableOpacity,
  TextComponent,
  findNodeHandle,
  UIManager,
  Dimensions,
  ScaledSize,
  PixelRatio,
  StyleSheet,
  StatusBar,
  Platform
} from "react-native";
import { Button, Text, Overlay, Image } from "react-native-elements";
import DeviceInfo from "react-native-device-info";
import { PortalDestination, PortalOrigin } from "rn-native-portals";
import { NavigationScreenProp } from "react-navigation";
import { YouTubeViewer } from "../YouTube/YouTube";
import YouTube from "react-native-youtube";
import Pdf from "react-native-pdf";
import { Immersive } from "react-native-immersive";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}
interface State {
  currentBlock: "blue" | "red";
  shouldMove: boolean;
  window: ScaledSize;
  containerMounted: boolean;
  containerWidth: number;
}
export class NativePortals extends React.Component<Props, State> {
  static navigationOptions = {
    header: null
  };

  private _textTag: number | null = 0;
  private _blueTag: number | null = 0;
  private _redTag: number | null = 0;

  private pdf: any;

  constructor(props) {
    super(props);
    this.state = {
      currentBlock: "red",
      shouldMove: false,
      window: Dimensions.get("window"),
      containerMounted: false,
      containerWidth: 0
    };
  }

  private getTextRef = (ref: any) => {
    this._textTag = findNodeHandle(ref);
  };

  private getBlueRef = (ref: any) => {
    this._blueTag = findNodeHandle(ref);
  };

  private getRedRef = (ref: any) => {
    this._redTag = findNodeHandle(ref);
  };

  private PortalText: JSX.Element = (
    <Text ref={this.getTextRef} style={{ color: "white" }}>
      Hello
    </Text>
  );

  public componentDidMount() {
    Dimensions.addEventListener("change", this.onChange);
    if (Platform.OS === "android") {
      Immersive.addImmersiveListener(this.restoreImmersive);
    }
    /*this.subscription = Device.subscribeToDimensionChanges(dims =>
      this.setState(dims)
      );*/
  }

  public componentWillUnmount() {
    Dimensions.removeEventListener("change", this.onChange);
    if (Platform.OS === "android") {
      Immersive.removeImmersiveListener(this.restoreImmersive);
    }
    // this.subscription.unsubscribe();
  }

  public componentDidUpdate(prevProps: Props, prevState: State) {
    const isLandscape: boolean = DeviceInfo.isLandscape();
    if (prevState.window.width !== this.state.window.width) {
      this.setFullScreen(isLandscape);
      // setTimeout(this.setFullScreen, 2000, isLandscape);
    }
  }

  private onChange = (dims: { window: ScaledSize }) => this.setState(dims);

  // listener for Immersive State recover
  public restoreImmersive = () => {
    __DEV__ && console.warn("Immersive State Changed!");
    const isLandscape: boolean = DeviceInfo.isLandscape();
    if (isLandscape) {
      console.log("restore immersive");
      Immersive.on();
      Immersive.setImmersive(true);
    }
  };
  /**
   * Interface for adding/removing/moving views within a parent view from JS.
   *
   * @param viewTag the view tag of the parent view
   * @param moveFrom a list of indices in the parent view to move views from
   * @param moveTo parallel to moveFrom, a list of indices in the parent view to move views to
   * @param addChildTags a list of tags of views to add to the parent
   * @param addAtIndices parallel to addChildTags, a list of indices to insert those children at
   * @param removeFrom a list of indices of views to permanently remove. The memory for the
   *     corresponding views and data structures should be reclaimed.
   */
  private removeChildren = (parentTag: number | null): void => {
    // Permute child at indice 0 and 1 of parent tag 6
    // UIManager.manageChildren(parentTag, [], [], [], [], [0]);
    // UIManager.removeSubviewsFromContainerWithID(parentTag);
  };

  private addChildren = (
    parentTag: number | null,
    childTag: number | null
  ): void => {
    // Permute child at indice 0 and 1 of parent tag 6
    // UIManager.manageChildren(parentTag, [], [], [childTag], [0], []);
    // UIManager.setChildren(parentTag, [childTag])
  };

  private onAddButtonPress = () => {
    this.addChildren(this._redTag, this._textTag);
  };

  private onRemoveButtonPress = () => {
    this.removeChildren(this._blueTag);
  };

  private onSwitchButtonPress = () => {
    this.switchBlockState();
  };

  private navigateToFullScreen = (): void => {
    this.props.navigation.navigate("ModalPortals");
  };

  private switchBlockState = (): void => {
    const { currentBlock, shouldMove } = this.state;
    this.setState({
      currentBlock: currentBlock === "blue" ? "red" : "blue",
      shouldMove: shouldMove ? false : true
    });
  };

  private setFullScreen = (isOn: boolean): void => {
    if (Platform.OS !== "android") {
      return;
    }
    console.log("set fullscreen: ", isOn);
    if (isOn) {
      Immersive.on();
    } else {
      Immersive.off();
    }
    StatusBar.setHidden(isOn);
    Immersive.setImmersive(isOn);
  };

  private renderYoutube(): JSX.Element {
    return (
      <YouTube
        apiKey="AIzaSyAr_DNlR7k4zzqS5_F8wcHAcHVEady9d7M"
        videoId="tGgSI5avEz4" // The YouTube video ID
        play // control playback of video with true/false
        loop // control whether the video should loop when ended
        onReady={e => console.log("youtube ready", e)}
        onChangeState={e => console.log("youtube change state", e)}
        onChangeQuality={e => console.log("youtube change quality", e)}
        onError={e => console.log("youtube error", e)}
        style={[
          {
            height: PixelRatio.roundToNearestPixel(
              this.state.containerWidth / (16 / 9)
            ),
            width: this.state.containerWidth,
            alignSelf: "stretch"
          }
        ]}
      />
    );
  }

  private renderPdf = (): JSX.Element => {
    console.log("render pdf");
    let source = {
      uri:
        "https://www.leshouches.com/img/sitra/fiche/820526-tmb-fiche-smbt.cd74.pdf",
      cache: true
    };
    return (
      <Pdf
        ref={pdf => {
          this.pdf = pdf;
        }}
        source={source}
        //page={this.props.page}
        //scale={this.props.scale}
        //enablePaging={false}
        // horizontal={true}
        onLoadComplete={(numberOfPages, filePath) => {
          //this.props.numberOfPages = numberOfPages; //do not use setState, it will cause re-render
          console.log(`total page count: ${numberOfPages}`);
        }}
        onPageChanged={(page, numberOfPages) => {
          //this.props.page = page; //do not use setState, it will cause re-render
          console.log(`current page: ${page}`);
        }}
        onError={error => {
          console.log(error);
        }}
        onPageSingleTap={(page: number) => {
          console.log(page);
        }}
        style={[styles.pdf]}
      />
    );
  };

  private renderImage = (): JSX.Element => {
    const { currentBlock, containerWidth } = this.state;
    const newSize: number = 150;
    return (
      <Image
        source={{ uri: "https://robohash.org/123" }}
        style={{
          width: newSize,
          height: newSize,
          borderRadius: 75
        }}
      />
    );
  };

  private renderContent = (): JSX.Element => {
    const isLandscape: boolean = DeviceInfo.isLandscape();
    //return this.renderPdf();
    // return <React.Fragment>{this.PortalText}</React.Fragment>;
    return this.renderImage();
  };

  public render() {
    const isLandscape: boolean = DeviceInfo.isLandscape();
    const { window, shouldMove, currentBlock } = this.state;
    console.log(
      "NativePortals render - isLandscape: ",
      isLandscape,
      this.state
    );
    // const pText: JSX.Element = <Text style={{ color: 'white' }}>Hello</Text>

    const destination: string | null =
      currentBlock === "red" ? null : "targetOfTeleportation2";
    /*
    const destination: string | null = isLandscape
      ? 'targetOfTeleportation'
      : null;
    */
    return (
      <React.Fragment>
        <View style={{ flex: 1 }}>
          <View
            ref={this.getBlueRef}
            style={[
              {
                flex: 1,
                margin: 2,
                flexDirection: "row"
              }
            ]}
            onLayout={({
              nativeEvent: {
                layout: { width }
              }
            }) => {
              if (!this.state.containerMounted)
                this.setState({ containerMounted: true });

              if (this.state.containerWidth !== width)
                this.setState({
                  containerWidth: width
                });
            }}
          >
            <View
              style={{
                flex: 1,
                width: "100%",
                backgroundColor: "blue",
                justifyContent: "flex-end"
              }}
            >
              <PortalDestination
                name="targetOfTeleportation2"
                style={{ flex: 1, width: "100%", height: "100%" }}
              />
            </View>
            <View
              style={[
                {
                  flex: 1,
                  backgroundColor: "red"
                }
              ]}
            >
              <PortalOrigin destination={destination} style={{ flex: 1 }}>
                {this.state.containerMounted && this.renderContent()}
              </PortalOrigin>
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: "center" }}>
            <Button title="Switch" onPress={this.onSwitchButtonPress} />
            <Button
              title="Force render"
              onPress={() => {
                this.forceUpdate();
              }}
            />
          </View>
        </View>

        {/* <Overlay
          isVisible={isLandscape}
          fullScreen={true}
          overlayBackgroundColor="black"
          onShow={() => {
            console.log('on Modal show');
          }}
          onDismiss={() => {
            console.log('on Modal Dismiss');
          }}
          onOrientationChange={() => {
            console.log('on Modal Orientation change');
          }}
        >
          <PortalDestination name="targetOfTeleportation" />
        </Overlay> */}

        <View
          style={{
            flex: 1,
            width: isLandscape ? "100%" : "0%",
            height: isLandscape ? "100%" : "0%",
            backgroundColor: "black",
            position: "absolute",
            opacity: isLandscape ? 1 : 0,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <PortalDestination
            style={{ flex: 1, width: "100%", height: "100%" }}
            name="targetOfTeleportation"
          />
        </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  pdfContainer: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 25
  },
  pdf: {
    flex: 1,
    alignContent: "center",
    height: "100%",
    width: "100%"
  }
});
