import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FBFAFA",
        flexDirection: 'column',
        justifyContent: "flex-start",
        alignItems: "center"
    },
    subcontainer: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center'
    }
});
export default styles;
