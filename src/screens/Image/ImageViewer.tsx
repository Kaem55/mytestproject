import React, { ReactText, ReactElement } from "react";
import { View, ScrollView, Text } from "react-native";
import styles from "./ImageViewerStyles";
import { Image, Avatar } from "react-native-elements";


interface Props { }
interface State { }


export class ImageViewer extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <Text>Image</Text>
                <View style={styles.subcontainer}>
                    <Image source={{ uri: "https://robohash.org/123" }} style={{ width: 150, height: 150, borderRadius: 75 }} />
                    <Image source={require('./../../images/avatar-default.png')} style={{ width: 150, height: 150, borderRadius: 75, overflow: 'hidden' }} />
                </View>
                <View style={styles.subcontainer}>
                    <Image source={{ uri: "https://robohash.org/222?set=set4" }} style={{ width: 150, height: 150, borderRadius: 75 }} />
                    <Image source={require('./../../images/avatar-default.png')} style={{ width: 150, height: 150, borderRadius: 75, overflow: 'hidden' }} />
                </View>
                <View style={styles.subcontainer}>
                    <Avatar source={{ uri: "https://robohash.org/1447" }} rounded={true} containerStyle={{ width: 150, height: 150 }} />
                    <Avatar source={require('./../../images/avatar-default.png')} placeholderStyle={{ backgroundColor: 'white' }} avatarStyle={{ borderRadius: 75, overflow: 'hidden', backgroundColor: 'transparent' }} containerStyle={{ width: 150, height: 150 }} />
                </View>
                <View style={styles.subcontainer}>
                    <Avatar source={require('./../../images/air-glowbl.png')} rounded={true} containerStyle={{ width: 150, height: 150 }} />
                    <Avatar source={require('./../../images/avatar-default.png')} overlayContainerStyle={{ backgroundColor: 'transparent' }} avatarStyle={{ borderRadius: 75, overflow: 'hidden' }} containerStyle={{ width: 150, height: 150 }} />
                </View>
            </ScrollView>
        );
    }



}