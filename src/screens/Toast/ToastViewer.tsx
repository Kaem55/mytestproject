import React from "react";
import { View, Switch, Platform, Text } from "react-native";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { showToast, closeToast } from "../../actions/toastActions";

interface IDispatchFromProps {
  showToast(time?: number, useNative?: boolean): void;
  closeToast(): void;
}

interface IState {
  useNativeToast: boolean;
}

type IProps = IDispatchFromProps;

class ToastViewer extends React.Component<IProps, IState> {
  public constructor(props) {
    super(props);
    this.state = {
      useNativeToast: false
    };
  }

  private onNativeSwitchChange = (value: boolean) => {
    this.setState({ useNativeToast: value });
  };

  private onShowButtonPress = () => {
    this.props.showToast(1000, this.state.useNativeToast);
  };

  private onShowPermanentButtonPress = () => {
    this.props.showToast(0, this.state.useNativeToast);
  };

  private onCloseButtonPress = () => {
    this.props.closeToast();
  };

  public render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "space-around",
          alignItems: "center",
          backgroundColor: "white",
          padding: 20
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <Text> use Native</Text>
          <Switch
            value={this.state.useNativeToast}
            onValueChange={this.onNativeSwitchChange}
          />
        </View>
        <Button title="show toast" onPress={this.onShowButtonPress} />
        <Button
          title="show permanent toast"
          onPress={this.onShowPermanentButtonPress}
        />
        <Button
          buttonStyle={{ backgroundColor: "red" }}
          title="hide toast"
          onPress={this.onCloseButtonPress}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  showToast: (time, useNative) => dispatch(showToast(time, useNative)),
  closeToast: () => dispatch(closeToast())
});

export default connect<{}, IDispatchFromProps>(
  null,
  mapDispatchToProps
)(ToastViewer);
