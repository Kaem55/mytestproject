import { applyMiddleware, compose, StoreCreator, createStore } from "redux";
import rootReducer from "./reducers/rootReducer";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas/rootSaga";
import logger from "redux-logger";
import SocketConnection from "./services/socketConnection";
import { NavigatorMiddleware } from "./navigator";
import reduxReset from "redux-reset";
import "./ReactotronConfig";
import Reactotron from "reactotron-react-native";

let opts = {};
const sagaMonitor = Reactotron.createSagaMonitor;
opts = { sagaMonitor };
const sagaMiddleware = createSagaMiddleware(opts);

export default function configureStore(): any {
  //const middlewares[:any] = [sagaMiddleware, logger]

  const enhancer = compose(
    applyMiddleware(sagaMiddleware, NavigatorMiddleware),
    reduxReset()
  );
  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore: StoreCreator = Reactotron.createStore; // createStore;
  const store = createAppropriateStore(rootReducer, enhancer);
  const connection: SocketConnection = new SocketConnection(store.dispatch);
  sagaMiddleware.run(rootSaga, connection);
  return store;
}
