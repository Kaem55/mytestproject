import {
  LOGIN_FETCHING_DATA_SUCCESS,
  LOGIN_FETCHING_DATA_FAILURE
} from "../constants/actionTypes";
const initialState = {
  data: String,
  isLoading: true
};

export default function(state = initialState, action: any) {
  console.log("reducer type: ", action.type);
  if (action.type === LOGIN_FETCHING_DATA_SUCCESS) {
    return {
      ...state,
      data: action.data
    };
  }
  if (action.type === LOGIN_FETCHING_DATA_FAILURE) {
    return {
      ...state,
      isLoading: false
    };
  }
  return state;
}
