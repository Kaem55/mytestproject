import { combineReducers } from "redux";
import homeReducer from "../reducers/homeReducer";
import loginReducer from "../reducers/loginReducer";
import chatUsersReducer from "../reducers/chatUsersReducer";
import chatMessagesReducer from "../reducers/chatMessagesReducer";
import { NavReducer } from "../navigator";
import pdfViewerReducer from "./pdfViewerReducer";

export default combineReducers({
  homeReducer,
  loginReducer,
  chatUsersReducer,
  chatMessagesReducer,
  pdfReducer: pdfViewerReducer,
  nav: NavReducer
});
