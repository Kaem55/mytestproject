import {
  HOME_FETCHING_DATA_SUCCESS,
  HOME_FETCHING_DATA_FAILURE
} from "../constants/actionTypes";
const initialState = {
  data: [],
  isLoading: true
};

export default function(state = initialState, action: any) {
  console.log("reducer type: ", action.type);
  if (action.type === HOME_FETCHING_DATA_SUCCESS) {
    return {
      ...state,
      data: action.data.users,
      isLoading: false
    };
  }
  if (action.type === HOME_FETCHING_DATA_FAILURE) {
    return {
      ...state,
      isLoading: false
    };
  }
  return state;
}
