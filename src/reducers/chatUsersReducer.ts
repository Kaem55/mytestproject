import * as types from "../constants/actionTypes";
const initialState = {
  name: "",
  id: "",
  users: []
};

export default function(state = initialState, action: any) {
  switch (action.type) {
    case types.ADD_USER:
      return {
        ...state,
        name: action.name,
        id: action.id
      };
    case types.USERS_LIST:
      return action.users;
    default:
      return state;
  }
}
