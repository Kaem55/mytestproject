import * as types from "../constants/actionTypes";

export interface IChatState {
  messages: any[];
  typing: string;
}

const initialState: IChatState = {
  messages: [],
  typing: ""
};

export default function(state = initialState, action: any) {
  switch (action.type) {
    case types.TYPING_CHANGED:
      return {
        ...state,
        typing: action.typing
      };
    case types.ADD_MESSAGE:
    case types.MESSAGE_RECEIVED:
      let newMessage: any = {
        message: action.message,
        author: action.author,
        avatar: action.avatar,
        id: action.id
      };
      //let newMessages: any[] = state.messages;
      //newMessages.push(newMessage);
      //state.messages.push(newMessage);
      return {
        ...state,
        messages: state.messages.concat(newMessage) //state.messages //state.messages.push(newMessage)
      };
    default:
      return state;
  }
}
