import { RootNavigator } from "../navigator";
import { NavigationActions } from "react-navigation";
// Start with two routes: The Main screen, with the Login screen on top.
const firstAction: any = RootNavigator.router.getActionForPathAndParams("Home");
const tempNavState = RootNavigator.router.getStateForAction(firstAction);
const secondAction: any = RootNavigator.router.getActionForPathAndParams(
  "Login"
);
const thirdAction: any = RootNavigator.router.getActionForPathAndParams("Chat");
const initialNavState = RootNavigator.router.getStateForAction(
  secondAction,
  tempNavState
);

function nav(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case "Login":
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.back(),
        state
      );
      break;
    case "Logout":
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: "Login" }),
        state
      );
      break;
    case "OpenChat":
      break;
    case "CloseChat":
      break;
    default:
      nextState = RootNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
