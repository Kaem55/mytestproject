import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  containerGridFullSquare: {
    justifyContent: "space-around",
    alignItems: "center",
    flex: 1
    // backgroundColor: '#00a1f1'
  },
  containerGridSquare: {
    justifyContent: "space-around",
    alignItems: "center",
    flex: 1
    // backgroundColor: '#ffbb00',
  },
  containerGridColumn: {
    justifyContent: "space-around",
    alignItems: "center",
    flex: 1,
    flexDirection: "column",
    flexBasis: "100%"
    // backgroundColor: '#7cbd00',
  },
  containerGridRow: {
    justifyContent: "space-around",
    alignItems: "center",
    flex: 1,
    flexDirection: "row",
    flexBasis: "100%"
    // backgroundColor: '#7cbd00',
  }
});

export default styles;
