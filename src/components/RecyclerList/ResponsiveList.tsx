import React, { ReactText } from "react";
import {
  DataProvider,
  LayoutProvider,
  RecyclerListView
} from "recyclerlistview";
import {
  View,
  LayoutChangeEvent,
  Text,
  StyleProp,
  ViewStyle
} from "react-native";
import styles from "./ResponsiveStyles";

const ViewTypes = {
  FULL: 0,
  FULLSQUARE: 1,
  COLUMN: 2,
  SQUARE: 3,
  ROW: 4
};

const dataProvider: DataProvider = new DataProvider((r1, r2) => {
  return r1 !== r2;
});

let containerCount = 0;

interface ICellProps {
  style?: StyleProp<ViewStyle>;
  width?: number;
  height?: number;
}

interface ICellState {
  width?: number;
  height?: number;
  mounted: boolean;
}

export interface InjectedProps {
  parentWidth: number;
  parentHeight: number;
}

class CellContainer extends React.PureComponent<ICellProps, ICellState> {
  private _containerId: number;
  constructor(args) {
    super(args);
    this._containerId = containerCount++;
    this.state = {
      mounted: false,
      width: 0,
      height: 0
    };
  }

  private onLayoutChanged = (event: LayoutChangeEvent): void => {
    const newWidth: number = Math.round(event.nativeEvent.layout.width);
    const newHeight: number = Math.round(event.nativeEvent.layout.height);
    if (this.state.width !== newWidth) {
      if (!this.state.mounted) this.setState({ mounted: true });
      this.setState({ width: newWidth, height: newHeight });
    }
  };

  public render(): JSX.Element {
    console.log("CellContainer render", this.props, this.state);
    const children = React.Children.map(this.props.children, (child, index) => {
      return React.cloneElement(
        child as React.ReactElement<any>,
        {
          parentWidth: this.state.width,
          parentHeight: this.state.height
        } as InjectedProps
      );
    });
    return (
      <View {...this.props} onLayout={this.onLayoutChanged}>
        {this.state.mounted && children}
      </View>
    );
  }
}

interface Props {
  style?: StyleProp<ViewStyle>;
  lenght?: number;
  manageLandscape: boolean;
  isLandscape: boolean;
  logsEnabled?: boolean;
}

interface State {
  mounted: boolean;
  width: number;
  height: number;
  dataProvider: DataProvider;
}

interface IDataItem {
  key: number;
  color: string;
}

export default class ResponsiveList extends React.PureComponent<Props, State> {
  private _layoutProvider: LayoutProvider;
  private _list: RecyclerListView;

  constructor(args) {
    super(args);
    this.state = {
      mounted: false,
      width: 0,
      height: 0,
      dataProvider: dataProvider.cloneWithRows(
        this.generateArray(this.props.lenght ? this.props.lenght : 4)
      )
    };
    // Create the data provider and provide method which takes in two rows of data and return if those two are different or not.
    // THIS IS VERY IMPORTANT, FORGET PERFORMANCE IF THIS IS MESSED UP
    // dataProvider = new DataProvider((r1, r2) => {
    //  return r1 !== r2;
    // });

    // Create the layout provider
    // First method: Given an index return the type of item e.g ListItemType1, ListItemType2 in case you have variety of items in your list/grid
    // Second: Given a type and object set the exact height and width for that type on given object, if you're using non deterministic rendering   provide close estimates
    // If you need data based check you can access your data provider here
    // You'll need data in most cases, we don't provide it by default to enable things like data virtualization in the future
    // NOTE: For complex lists LayoutProvider will also be complex it would then make sense to move it to a different file
    this._layoutProvider = new LayoutProvider(
      index => {
        if (this.props.isLandscape && this.props.manageLandscape) {
          return ViewTypes.ROW;
        }
        if (this.state.dataProvider.getSize() === 1) {
          return ViewTypes.FULL;
        }
        if (this.state.dataProvider.getSize() === 2) {
          return ViewTypes.FULLSQUARE;
        }
        if (this.state.dataProvider.getSize() === 3) {
          return ViewTypes.COLUMN;
        }
        return ViewTypes.SQUARE;
      },
      (type, dim) => {
        switch (type) {
          case ViewTypes.SQUARE:
            dim.width = Math.round(this.state.width / 2); // Metrics.screenWidth / 2;
            dim.height = Math.round(this.state.height / 2) - 1; // 160;
            break;
          case ViewTypes.FULLSQUARE:
            dim.width = this.state.width / 2; // Metrics.screenWidth / 2;
            dim.height = this.state.height; // 320;
            break;
          case ViewTypes.FULL:
            dim.width = this.state.width; // Metrics.screenWidth;
            dim.height = this.state.height; // 320;
            break;
          case ViewTypes.COLUMN:
            dim.width = this.state.width / 3; // Metrics.screenWidth / 3;
            dim.height = this.state.height; // 320;
            break;
          case ViewTypes.ROW:
            dim.width = this.state.width;
            dim.height = this.state.height / 3;
            break;
          default:
            dim.width = this.state.width / 2; // Metrics.screenWidth / 2;
            dim.height = this.state.height / 2; // 160;
        }
        if (this.props.logsEnabled) {
          console.log("layoutrenderer type", type, dim);
        }
      }
    );

    this._rowRenderer = this._rowRenderer.bind(this);

    // Since component should always render once data has changed, make data provider part of the state
    /*     this.state = {
      dataProvider: this.dataProvider.cloneWithRows(dataArray)
    }; */
  }

  // Given type and data return the view component
  private _rowRenderer = (
    type: ReactText,
    data: IDataItem,
    index: number,
    extendedState?: any
  ): JSX.Element => {
    if (this.props.logsEnabled) {
      console.log("rowrenderer index", index);
    }
    // You can return any view here, CellContainer has no special significance
    switch (type) {
      case ViewTypes.SQUARE:
      case ViewTypes.ROW:
        return (
          <CellContainer
            style={[
              styles.containerGridSquare,
              { backgroundColor: data.color }
            ]}
          >
            <Text>Data: {data.key}</Text>
          </CellContainer>
        );
      case ViewTypes.COLUMN:
        return (
          <CellContainer
            style={[
              styles.containerGridColumn,
              { backgroundColor: data.color }
            ]}
          >
            <Text>Data: {data.key}</Text>
          </CellContainer>
        );
      case ViewTypes.FULL:
      case ViewTypes.FULLSQUARE:
        return (
          <CellContainer
            style={[
              styles.containerGridFullSquare,
              { backgroundColor: data.color }
            ]}
          >
            <Text>Data: {data.key}</Text>
          </CellContainer>
        );
      default:
        return (
          <CellContainer style={styles.containerGridSquare}>
            <Text>Data: {data}</Text>
          </CellContainer>
        );
    }
  };

  private getListRef = (ref: any) => {
    this._list = ref;
  };

  private generateArray(n): Array<IDataItem> {
    let arr = new Array(n);
    for (let i = 0; i < n; i++) {
      arr[i] = { key: i, color: this.getRandomColor() };
    }
    return arr;
  }

  // function responsible to generate new random color
  private getRandomColor = (): string => {
    return (
      "rgb(" +
      Math.floor(Math.random() * 256) +
      "," +
      Math.floor(Math.random() * 256) +
      "," +
      Math.floor(Math.random() * 256) +
      ")"
    );
  };

  private onLayoutChanged = (event: LayoutChangeEvent): void => {
    const newWidth: number = Math.round(event.nativeEvent.layout.width);
    const newHeight: number = Math.round(event.nativeEvent.layout.height);
    if (this.props.logsEnabled) {
      console.log("onLayoutChanged", newWidth, newHeight);
    }
    if (this.state.width !== newWidth && newHeight > 100) {
      if (!this.state.mounted) this.setState({ mounted: true });
      this.setState({ width: newWidth, height: newHeight });
    }
  };

  public componentDidUpdate(prevProps, prevState: State, snapshot) {
    let dimensionChanged: boolean = prevState.width !== this.state.width;
    if (this.props.logsEnabled) {
      console.log("componentDidUpdate dimensionChanged ", dimensionChanged);
    }
    if (dimensionChanged && this._list) {
      this._list.forceUpdate();
    }
  }

  public render(): JSX.Element {
    const needVerticalScroll: boolean =
      this.props.isLandscape && this.props.manageLandscape;
    if (this.props.logsEnabled) {
      console.log("render isLandscape", this.props.isLandscape);
    }
    return (
      <View
        style={styles.container}
        {...this.props.style}
        onLayout={this.onLayoutChanged}
      >
        {this.state.mounted && this.state.dataProvider.getSize() > 0 && (
          <RecyclerListView
            ref={this.getListRef}
            canChangeSize={true}
            isHorizontal={!needVerticalScroll}
            layoutProvider={this._layoutProvider}
            rowRenderer={this._rowRenderer}
            dataProvider={this.state.dataProvider}
            forceNonDeterministicRendering={false}
            extendedState={{ width: 10, height: 10 }}
          />
        )}
      </View>
    );
  }
}
