import React from 'react';
import { View, StyleSheet, ViewStyle, StyleProp } from 'react-native';
interface IProps {
  backgroundColor: string;
  radius: number;
  style?: StyleProp<ViewStyle>;
}
export default class Circle extends React.Component<IProps> {
  static defaultProps = {
    backgroundColor: 'red',
    radius: 100,
    style: {}
  };

  constructor(args) {
    super(args);
  }
  public render(): JSX.Element {
    return (
      // <View {...this.props}>{this.props.children}</View>
      <View
        style={[
          styles.circle,
          this.props.style,
          {
            backgroundColor: this.props.backgroundColor,
            width: this.props.radius * 2,
            height: this.props.radius * 2,
            borderRadius: this.props.radius
          }
        ]}
      >
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  circle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    backgroundColor: 'red',
    overflow: 'hidden',
    zIndex: 1
  }
});
