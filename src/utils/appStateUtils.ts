import { AppState, AppStateStatus } from "react-native";

class AppStateWrapper {
  static addChangeListener(callBack: (state: AppStateStatus) => void): void {
    AppState.addEventListener("change", callBack);
  }

  static removeChangelistener(callBack: (state: AppStateStatus) => void): void {
    AppState.removeEventListener("change", callBack);
  }
}

export default AppStateWrapper;
