import Reactotron from "reactotron-react-native";

export enum DbgLevel {
  ALL = 0,
  DEBUG = 1,
  ERROR = 2,
  WARNING = 3,
  INFO = 4,
  NONE = 5
}

export abstract class Dbg {
  static level: DbgLevel = DbgLevel.ALL;
  static debug(message: any): void {
    if (Dbg.level <= DbgLevel.DEBUG) {
      // Reactotron.debug(message);
      console.debug(message);
    }
  }
  static info(message: any): void {
    if (Dbg.level <= DbgLevel.INFO) {
      // Reactotron.info(message);
      console.info(message);
    }
  }
  static warn(message: any): void {
    if (Dbg.level <= DbgLevel.WARNING) {
      // Reactotron.warn(message);
      console.warn(message);
    }
  }
  static error(message: any): void {
    if (Dbg.level <= DbgLevel.ERROR) {
      // Reactotron.error(message);
      console.error(message);
    }
  }
}
