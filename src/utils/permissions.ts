import { PermissionsAndroid } from "react-native";

async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures."
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

async function requestReadStoragePermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      {
        title: "need read Permission",
        message: "need read Permission"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the read permission");
    } else {
      console.log("read permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

async function requestWriteStoragePermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "need write Permission",
        message: "need write Permission"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the write permission");
    } else {
      console.log("write permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

async function requestReadWriteStoragePermission() {
  console.log("try request permission");
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    ]);

    console.log("request permission result: ", granted);
    /*
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the write permission");
    } else {
      console.log("write permission denied");
    }*/
  } catch (err) {
    console.warn(err);
  }
}

export default requestReadWriteStoragePermission;
