import { RouteNames } from "../constants/navigation";

export function buildScreensArray(): Object[] {
  return Object.keys(RouteNames).map(key => ({
    id: RouteNames[key],
    name: key
  }));
}
