package com.myreactproject;

import android.app.Application;
import com.facebook.react.ReactApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.wonday.pdf.RCTPdfView;
import com.mfrachet.rn.RNNativePortalsPackage;
import com.rnimmersive.RNImmersivePackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.oney.WebRTCModule.WebRTCModulePackage; 
import com.ocetnik.timer.BackgroundTimerPackage;
import com.zxcpoiu.incallmanager.InCallManagerPackage;
// import com.github.droibit.android.reactnative.customtabs.CustomTabsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNDeviceInfo(),
            new AsyncStoragePackage(),
            new ReactNativeYouTube(),
            new RNGestureHandlerPackage(),
            new VectorIconsPackage(),
            new RNFetchBlobPackage(),
            new RCTPdfView(),
            new BackgroundTimerPackage(),
            new InCallManagerPackage(),
            new RNImmersivePackage(),
            new RNNativePortalsPackage(),
            // new CustomTabsPackage(),
            new WebRTCModulePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
