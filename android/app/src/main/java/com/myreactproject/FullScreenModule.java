package com.myreactproject;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

public class FullScreenModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    FullScreenModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "FullScreen";
    }

    public void setVisibility(boolean isOn){

    }

}